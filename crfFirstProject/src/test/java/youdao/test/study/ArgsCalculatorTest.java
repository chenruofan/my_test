package youdao.test.study;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ArgsCalculatorTest {
    ArgsCalculator calculator;

    @BeforeMethod
    public void setUp() throws Exception {
        ArgsCalculator calculator = new ArgsCalculator();
    }

    @AfterMethod
    public void tearDown() throws Exception {
    }

    @Test
    public void testSum() throws Exception {
        int result = calculator.sum(3, 4);
        assertEquals(7, result);
    }

    @Test
    public void testSubtraction() throws Exception {
        int result = 10 - 3;
        assertTrue(result == 9);
    }

    @Test @Deprecated
    public void testMultiplication() throws Exception {
    }

    @Test
    public void testDivison() throws Exception {
        try {
            int result = calculator.divison(10, 2);
            assertEquals(5, result);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    @Test
    public void testEqualIntegers() throws Exception {
        boolean result = calculator.equalIntegers(20, 20);

        assertFalse(result);
    }

}